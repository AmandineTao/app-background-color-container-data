const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  // Get the background color from the environment variable, default to 'lightblue'
  const bgColor = process.env.BACKGROUND_COLOR || 'lightblue';

  // Get container information
  const containerInfo = {
    'Container ID': process.env.HOSTNAME || 'N/A',
    'IP Address': getIPAddress() || 'N/A',
  };

  // Create an HTML table with the container information
  const table = `
    <table style=" border-collapse: collapse; width: 50%; margin: auto;">
      <tr>
        <th style="padding: 10px; font-size: 60px; background-color: #f2f2f2; text-align: center;">Key</th>
        <th style="padding: 10px; font-size: 60px; background-color: #f2f2f2; text-align: center;">Value</th>
      </tr>
      ${Object.entries(containerInfo)
        .map(
          ([key, value]) => `<tr><td style="padding: 10px; font-size: 60px; text-align: center;">${key}</td><td style="padding: 10px; font-size: 60px;text-align: center;">${value}</td></tr>`
        )
        .join('')}
    </table>
  `;

  // Send the HTML response
  res.send(`
    <html>
      <head>
        <style>
          body {
            font-family: 'Arial', sans-serif;
            text-align: center;
            padding: 20px;
            background: ${bgColor}
          }
          h1 {
          font-size: 80px;
          margin-top: 60px;
          margin-bottom: 60px;
          color: white;
          }
        </style>
      </head>
      <body>
        <h1>Container Information</h1>
        ${table}
      </body>
    </html>
  `);
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});

// Function to get the IP address of the container using a shell command
function getIPAddress() {
  const { execSync } = require('child_process');

  try {
    // Execute the shell command to get the IP address
    const ipAddress = execSync('ip addr show eth0 | grep "inet\\b" | awk \'{print $2}\' | cut -d/ -f1', { encoding: 'utf-8' });

    // Remove trailing newline characters
    return ipAddress.trim();
  } catch (error) {
    console.error('Error getting IP address:', error.message);
    return null;
  }
}
